FROM python:3.12-bookworm

ARG GIT_COMMIT

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    GIT_COMMIT=${GIT_COMMIT}

# Set the working directory in the container
WORKDIR /app

# Copy only the dependency files first to leverage Docker cache
COPY README.md pyproject.toml poetry.lock gunicorn-runserver.sh ./

# Install poetry
RUN pip install poetry==1.8.2

# Install dependencies (create a virtual environnment)
RUN poetry install --no-root

# Copy the rest of the application
COPY src ./src

ENV PYTHONPATH=/app/src
ENV MYPYPATH=/app/src

# Expose the port on witch the Django app will run
EXPOSE 3000

CMD ["poetry", "run", "./gunicorn-runserver.sh"]
