#!/bin/bash

DJANGO_SERVICE="portfolio"

src_path="$(dirname "$(realpath "$0")")/src"

scripts_path="$(dirname "$(dirname "$(realpath "$0")")")/scripts"

CONTAINER_ID="$(docker ps -qf "ancestor=$DJANGO_SERVICE")"


function main() {
  case "$1" in
    "--help")
      echo "usage: ./manage.sh COMMAND [ARGS]"
      echo "Commands:"
      echo "    run: run the project"
      echo "    test: run the tests with no migrations, with parallel process and coverage"
      echo "          possible args: -k <test name>"
      echo "    test_debug: run the tests with no migrations and no coverage and no parallel process"
      echo "          possible args: -k <test name>"
      echo "    fmt: run ruff to check and format code"
      echo "    lint run ruff and mypy to check code quality"
      echo "    django: run djando command passed in argument"
      echo "          possible args: makemigrations, migrate <app_name>, startapp <app_name>"
      echo "    poetry: run poetry for packages managing. possible args:"
      echo "            show: show list of installed packages"
      echo "            add <package_name==version> --group <group_name>: to add package in pyproject.toml"
      echo "            install: install package no yet installed and update poetry.lock"
      echo "            remove <package_name>: to delete package"
      echo "            update: update all dependencies"
      echo "            update <package_name>^version_min: update package with version"
      echo "            https://python-poetry.org/docs/cli/#update"
      echo "    logs: run logs display for sobriscore conteneur, with possible options"
      echo "    shell_plus: run shell_plus with ipython (<TAB> completion)"

      ;;

    "run")
        shift;
        docker compose down -t 0
        docker compose up --build -d
        echo "Pensez à faire un ./manage.sh django migrate si vous avez des migrations à appliquer"
        ;;

    "django")
        shift;
        docker exec -it "$CONTAINER_ID" bash -c "cd src && poetry run python manage.py $*"
        ;;

    "logs")
        shift;
        docker logs "$CONTAINER_ID" -f
        ;;

    "poetry")
        shift;
        docker exec -it "$CONTAINER_ID" bash -c "poetry $*"
        ;;

    "shell_plus")
        shift;
        docker exec -it "$CONTAINER_ID" bash -c "cd src && poetry run python manage.py shell_plus --ipython $*"
        ;;

    "fmt")
        shift;
        docker exec -it "$CONTAINER_ID" bash -c "poetry run ruff check --fix . $*"
        echo "lint OK ✓"
        docker exec -it "$CONTAINER_ID" bash -c "poetry run ruff format ."
        echo "format OK ✓"
        ;;

    "lint")
        shift;
        docker exec -it "$CONTAINER_ID" bash -c "poetry run ruff check . $*"
        echo "lint OK ✓"
        docker exec -it "$CONTAINER_ID" bash -c "poetry run ruff format --check ."
        echo "format OK ✓"
        echo "mypy …"
        docker exec -it "$CONTAINER_ID" bash -c "poetry run mypy ."
        echo "typing OK ✓"
        ;;

    "test")
        shift;
        docker exec -it "$CONTAINER_ID" bash -c "poetry run pytest --no-migrations --cov=. $*"
        echo "To run tests with debug fonctionalities run ./manage.sh test_debug"
        ;;

    "test_debug")
        shift;
        docker exec -it "$CONTAINER_ID" bash -c "poetry run pytest --no-migrations $*"
        echo "To run tests with coverage run ./manage.sh test"
        ;;

    "gitcc")
        shift;
        "$scripts_path"/conventionnal-commit.sh "$@"
        ;;

    "nf")
        shift;
        "$scripts_path"/init-issue.sh "$@"


    esac
}

main "$@"
