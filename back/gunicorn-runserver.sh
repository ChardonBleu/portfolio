#!/bin/bash


function runserver() {
    timeout=${GUNICORN_TIMEOUT:-30}
    workers=${GUNICORN_WORKERS:-9}
    connections_per_worker=${GUNICORN_CONNECTIONS_PER_WORKER:-200}
    if [[ "$ENVIRONMENT" == "DEV" ]]; then
        AUTORELOAD="--reload"
    fi
    gunicorn project.wsgi -b 0.0.0.0:3000 --log-file=- --worker-tmp-dir=/dev/shm -t "$timeout" --worker-class gevent --workers="$workers" --worker-connections="$connections_per_worker" --capture-output $AUTORELOAD
}

runserver
