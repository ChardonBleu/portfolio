# **********************************************************************
#    poetry

[tool.poetry]
name = "back"
version = "0.1.0"
description = ""
authors = ["fabienne rondi <f.rondi@orange.fr>"]
readme = "README.md"

[tool.poetry.dependencies]
python = "^3.12"
django = "^5.0"
djangorestframework = "^3.15"
psycopg = "^3.1.18"
django-model-utils = "^4.4.0"
django-extensions = "^3.2.3"
ipython = "^8.28.0"
django-storages = "^1.14.2"
boto3 = "^1.34.73"
ruff = "^0.3.4"
pytest = "^8.1.1"
pytest-django = "^4.8.0"
pytest-cov = "^5.0.0"
factory-boy = "^3.3.0"
gunicorn = "^21.2.0"
gevent = "^24.2.1"
mypy = "1.7"
django-stubs = {version = "^4.2.7", extras = ["compatible-mypy"]}
djangorestframework-stubs = {version = "^3.14.5", extras = ["compatible-mypy"]}
pytest-timeout = "^2.3.1"
pytest-sugar = "^1.0.0"
pytest-xdist = "^3.5.0"
pillow = "^10.3.0"
types-factory-boy = "^0.4.1"
drf-spectacular = "^0.27.2"
django-silk = "^5.3.1"
sentry-sdk = {extras = ["django"], version = "^2.20.0"}


[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"

# =============================================================================
# mypy
# =============================================================================

[tool.mypy]
python_version = "3.12"

plugins = [
    "mypy_drf_plugin.main",
    "mypy_django_plugin.main",
]

[tool.django-stubs]

django_settings_module = "project.settings.dev"

[[tool.mypy.overrides]]
module = "model_utils."
ignore_missing_imports = true

[[tool.mypy.overrides]]
module = ".migrations."
ignore_errors = true  # Django migrations should not produce any errors

# =============================================================================
# pytest
# =============================================================================

[tool.pytest.ini_options]
DJANGO_SETTINGS_MODULE = "project.settings.dev"
addopts = "--no-cov-on-fail --cov-fail-under=100"
timeout = 0.6
timeout_func_only = true


[tool.coverage.run]
omit = [
    "*/migrations/*.py",
    "*__init__*.py",
    "*/tests/*",
    "*/project/*",
    "conftest.py"
]

# =============================================================================
# ruff
# =============================================================================

[tool.ruff]
# Assume Python 3.12
target-version = "py312"
# preview = true

# Rules: https://docs.astral.sh/ruff/rules/

[tool.ruff.lint]
select = [
    "F", # Pyflakes
    "E", # pycodestyle errors
    "W", # pycodestyle warnings
    "C90", # mccabe
    "I", # isort
    "N", # pep8-naming
    "UP", # pyupgrade
    "YTT", # flake8-2020
    "S", # bandit
    "BLE", # flake8-blind-except
    "B", # flake8-bugbear
    "A", # flake8-builtins
    "C4", # flake8-comprehensions
    "DTZ", # flake8-datetimez
    "DJ", # flake8-django
    "EM", # flake8-errmsg
    "EXE", # flake8-executable
    "ISC", # flake8-implicit-str-concat
    "ICN", # flake8-import-conventions
    "G", # flake8-logging-format
    "INP", # flake8-no-pep420
    "PIE", # flake8-pie
    "T20", # flake8-print
    "PYI", # flake8-pyi
    "PT", # flake8-pytest-style
    "RSE", # flake8-raise
    "RET", # flake8-return
    "SLF", # flake8-self
    "SLOT", # flake8-slots
    "SIM", # flake8-simplify
    "TID", # flake8-tidy-imports
    "TCH", # flake8-type-checking
    "PTH", # flake8-use-pathlib
    "ERA", # eradicate
    "PD", # pandas-vet
    "PL", # Pylint
    "TRY", # tryceratops
    "FLY", # flynt
    "NPY", # NumPy-specific rules
    "PERF", # Perflint
#   "FURB", # refurb [preview]
#   "LOG", # flake8-logging [preview]
    "RUF", # Ruff-specific rules
]

ignore = [
    "RUF012", # [mutable-class-default]
    "ISC001", # [single-line-implicit-string-concatenation]
]

[tool.ruff.lint.per-file-ignores]
"*/tests/*" = [
    "S101", # [security]  # assert
]


"manage.py" = [
    "TRY003",  # long exception messages
    "EM101",  # variable instead of raise exception directly
]

"**/settings/*" = [
    "E501",  # line too long
    "SIM210"  # Remove unnecessary `True if ... else False`
]

"**/migrations/" = [
    "N806",  # Variable in function should be lowercase
    "RUF001",  # String contains ambiguous ’ (RIGHT SINGLE QUOTATION MARK). Did you mean `` (GRAVE ACCENT)?
    "PLR0915",  # Too many statements
    "SLF001",  # Private member accessed: _meta`
]
