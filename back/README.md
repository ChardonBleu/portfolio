# PortFolio back-end  

## Clonage du repo Gitlab:

Cloner le repo en local.

## Ajout des variables d'environnement:
Créer un fichier .env dans le dossier back et le remplir avec ces données :

### Django
DJANGO_SECRET_KEY=`<Secret key django>` # 50 caractères
DJANGO_ALLOWED_HOSTS=`.localhost,.0.0.0.0`
DJANGO_CSRF_TRUSTED_ORIGINS=`http://localhost*,http//0.0.0.0*`  
DJANGO_SETTINGS_MODULE=`project.settings`  

### Admin
ADMIN_URL=`admin/`   # personnalisable, ex: secretadmin/  

### Postgres
POSTGRES_DB=`<Nom de la base de données postgres>`  
POSTGRES_USER=`<Nom de l'utilisateur postgres>`  
POSTGRES_PASSWORD=`<Mot de passe de l'utilisateur postgres>`  
POSTGRES_PORT=`5432`

### Global
ENVIRONMENT=`DEV`  

### Gunicorn
GUNICORN_TIMEOUT=`30`  
GUNICORN_WORKERS=`5`  
GUNICORN_CONNECTIONS_PER_WORKER=`40`

## Lancement de l'application :

Dans un Terminal se mettre dans le dossier back et faire : `./manage.sh run` pour lancer
les conteneurs docker python et postgres.  
Faire tourner les migrations django : `./manage.sh django migrate` puis créer un
utilisateur superuser : `./manage.sh django createsuperuser`.  

Vous pouvez alors accéder au panel admin avec les identifiants du super user créé :
http://127.0.0.1:3000/admin/
(adaptez l'url si vous l'avez personnalisé avec la variable d'environnement).  

Vous pouvez également accéder à l'api dans l'interfance django rest :
http://127.0.0.1:3000/api/

Seules les méthodes `get` sont autorisées sur l'api. Les modifications de donnée dans 
la base de donnée ne peuvent se faire que par le panel d'administration.

Toutes les commandes de gestion sont commentées dans [manage.sh](manage.sh) file.

# API documentation

The librairy drf-spectacular provides api schema:
http://127.0.0.1:3000/api/schema/redoc/



# Installation d'un interpréteur sur Pycharm:

Pour ne pas voler tous les honneurs, c'est fortement inspiré de 
cette [documentation](https://gist.github.com/jmsalcido/d89f4fa5059867816000220eebe0d95c)
et de [celle-ci](https://www.youtube.com/watch?v=xvFZjo5PgG0)

## Étape 1 :

Tout d'abord, il va falloir vous connecter à l'intérieur du conteneur docker.

Pour cela, il faut ouvrir un terminal dans Pycharm (en bas a gauche) et taper la 
commande suivante :

`docker ps`

Cela devrait vous donner un résultat similaire à celui-ci :

![](./resources/install-pycharm/dockerps.png)

Copiez l'identifiant à gauche du nom du conteneur (ici `back_python`) et tapez la commande suivante :

`docker exec -it <id> bash` ou `<id>` correspond à l'identifiant du container.

Vous êtes maintenant dans le conteneur. Faites `poetry env info -p` et récupérez le 
chemin indiqué et mettez-le de côté, vous en aurez besoin plus tard.


## Étape 2 :

Dans **Settings** > **Project** > **Python Interpreter** > **Show all** > **Add**

![](./resources/install-pycharm/interpreter.png)

Sélectionnez **Docker Compose**, cela devrait vous ouvrir cette fenêtre :

![](./resources/install-pycharm/docker-compose.png)

Dans _configuration files_ sélectionnez le fichier `docker-compose.yml` de la partie back du projet.

Pour la partie _service_ sélectionnez `python`

Pour le _project name_ vous pouvez mettre ce que vous voulez (`port-folio` par ex)

Cela devrait vous emmener sur cette fenêtre :

![](./resources/install-pycharm/runtime.png)

C'est ici que vous allez utilisez le chemin récupéré à l'étape 1.

Cliquez sur les 3 points sur la droite, collez le chemin que vous avez récupéré à l'étape 1 et ajoutez `/bin/python` à la fin.

Validez et maintenant tout est ok ! Vous avez un interpréteur ready ! 
