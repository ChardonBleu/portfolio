from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import gettext_lazy as _


class Profile(models.Model):
    birth_date = models.DateField(verbose_name=_("Date de naissance"))
    github_link = models.URLField(verbose_name=_("lien Git-hub"))
    gitlab_link = models.URLField(verbose_name=_("lien Gitlab"))
    codingame_link = models.URLField(verbose_name=_("lien Codingame"))
    linkedin_link = models.URLField(verbose_name=_("lien LinkedIn"))
    user = models.OneToOneField(
        to=User, verbose_name=_("utilisateur"), on_delete=models.CASCADE
    )

    def __str__(self) -> str:
        return f"{self.user.first_name} {self.user.last_name}"


class Training(models.Model):
    title = models.CharField(
        verbose_name=_("Formation"),
        max_length=250,
    )
    organism = models.CharField(verbose_name=_("organisme de formation"))
    delivery_date = models.DateField(
        verbose_name=_("date d'obtention"), blank=True, null=True
    )
    profile = models.ForeignKey(
        "Profile", verbose_name=_("profil"), on_delete=models.CASCADE
    )
    site = models.CharField(verbose_name=_("Lieu de formation"))
    learned_skills = models.CharField(verbose_name=_("compétences travaillées"))
    logo = models.ImageField(verbose_name=_("logo de l'organisme"), null=True)

    class Meta:
        ordering = ["delivery_date"]

    def __str__(self) -> str:
        date = self.delivery_date if self.delivery_date else "En cours"
        return f"{self.title} {date}"


class Experience(models.Model):
    class TypeJob(models.TextChoices):
        CDD = "CDD", "CDD"
        CDI = "CDI", "CDI"
        STAGE = "STAGE", "Stage"

    class SiteJob(models.TextChoices):
        SITE = "Sur site", "Sur Site"
        HYBRID = "Hybride", "Hybride"
        REMOTE = "A distance", "A distance"

    job = models.CharField(
        verbose_name=_("Poste"),
        max_length=255,
    )
    type = models.CharField(
        verbose_name=_("Type d'emploi"), max_length=255, choices=TypeJob.choices
    )
    firm = models.CharField(verbose_name=_("Nom de l'entreprise"), max_length=255)
    details = models.TextField(verbose_name=_("Détails du poste"), blank=True)
    start_date = models.DateField(verbose_name=_("date de prise de poste"))
    end_date = models.DateField(
        verbose_name=_("date de fin de contrat"), blank=True, null=True
    )
    profile = models.ForeignKey(
        "Profile", verbose_name=_("profil"), on_delete=models.CASCADE
    )
    site = models.CharField(
        verbose_name=_("Lieu d'emploi"),
        max_length=255,
        choices=SiteJob.choices,
    )

    class Meta:
        ordering = ["start_date"]

    def __str__(self) -> str:
        end_date = self.end_date if self.end_date else "aujourd'hui"
        return f"{self.job} de {self.start_date} à {end_date}"


class Skill(models.Model):
    class Category(models.TextChoices):
        BACK = "Back-end", "Back-end"
        FRONT = "Front-end", "Front-end"
        OTHER = "Autre", "Autre"

    title = models.CharField(
        verbose_name=_("Compétence"),
        max_length=250,
    )
    competence_level = models.IntegerField(
        verbose_name=_("niveau de compétence (sur 10)")
    )
    appetite_level = models.IntegerField(verbose_name=_("niveau d'appétence (sur 10)"))
    profile = models.ForeignKey(
        "Profile", verbose_name=_("profil"), on_delete=models.CASCADE
    )
    category = models.CharField(
        verbose_name=_("catégorie"),
        max_length=250,
        choices=Category.choices,
    )
    on_cv = models.BooleanField(verbose_name=_("visible sur le CV"), default=True)

    class Meta:
        ordering = ["-competence_level", "-appetite_level"]

    def __str__(self) -> str:
        return f"{self.title}"


class Project(models.Model):
    class Category(models.TextChoices):
        PRO = "Professionnel", "Projet professionnel"
        PERSO = "Personnel", "Projet personnel"
        TRAINING = "Formation", "Projet de formation"

    title = models.CharField(
        verbose_name=_("Nom du projet"),
        max_length=250,
    )
    description = models.TextField(verbose_name=_("Description du projet"))
    github_link = models.URLField(verbose_name=_("lien Git-hub ou Gitlab"), blank=True)
    app_link = models.URLField(verbose_name=_("lien vers l'application"), blank=True)
    profile = models.ForeignKey(
        "Profile", verbose_name=_("profil"), on_delete=models.CASCADE
    )

    skills = models.ManyToManyField(
        Skill, verbose_name=_("Techno utilisées"), blank=True, default=None
    )
    experience = models.ForeignKey(
        "Experience",
        related_name="projects",
        verbose_name=_("Contexte professionnel"),
        blank=True,
        on_delete=models.CASCADE,
        null=True,
    )
    category = models.CharField(
        verbose_name=_("Type de projet"),
        max_length=250,
        choices=Category.choices,
        blank=True,
    )
    year = models.CharField(
        verbose_name=_("Année de travail sur le projet"), blank=True, default=""
    )

    def __str__(self) -> str:
        return f"{self.title}"


class ProjectImage(models.Model):
    image = models.ImageField(verbose_name=_("Image"))
    name = models.CharField(verbose_name=_("nom de l'image"), max_length=250)
    project = models.ForeignKey(
        "Project",
        related_name="images",
        verbose_name=_("Projet"),
        on_delete=models.CASCADE,
    )

    def __str__(self) -> str:
        return f"{self.name}"
