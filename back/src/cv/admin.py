from django.contrib import admin

from .models import Experience, Profile, Project, ProjectImage, Skill, Training


class SkillAdmin(admin.ModelAdmin):
    list_display = ("title", "competence_level", "appetite_level", "on_cv")


admin.site.register(Profile)
admin.site.register(Experience)
admin.site.register(Training)
admin.site.register(Project)
admin.site.register(Skill, SkillAdmin)
admin.site.register(ProjectImage)
