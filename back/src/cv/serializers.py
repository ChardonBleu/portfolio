from rest_framework import serializers

from .models import Experience, Profile, Project, ProjectImage, Skill, Training


class ProfileSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(source="user.first_name")
    last_name = serializers.CharField(source="user.last_name")
    email = serializers.EmailField(source="user.email")

    class Meta:
        model = Profile
        fields = [
            "id",
            "first_name",
            "last_name",
            "email",
            "birth_date",
            "github_link",
            "gitlab_link",
            "codingame_link",
            "linkedin_link",
        ]


class TrainingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Training
        fields = [
            "id",
            "title",
            "organism",
            "delivery_date",
            "site",
            "learned_skills",
            "logo",
        ]


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skill
        fields = [
            "id",
            "title",
            "competence_level",
            "appetite_level",
            "category",
            "on_cv",
        ]


class ProjectImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectImage
        fields = [
            "id",
            "image",
            "name",
            "project",
        ]


class ProjectSerializer(serializers.ModelSerializer):
    skills = SkillSerializer(read_only=True, many=True)
    images = ProjectImageSerializer(read_only=True, many=True)

    class Meta:
        model = Project
        fields = [
            "id",
            "title",
            "description",
            "github_link",
            "app_link",
            "category",
            "year",
            "experience",
            "skills",
            "images",
        ]


class ExperienceSerializer(serializers.ModelSerializer):
    projects = ProjectSerializer(read_only=True, many=True)

    class Meta:
        model = Experience
        fields = [
            "id",
            "job",
            "type",
            "firm",
            "site",
            "details",
            "start_date",
            "end_date",
            "projects",
        ]
