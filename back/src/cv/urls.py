from django.urls import include, path
from rest_framework import routers

from .views import (
    ExperienceViewSet,
    ProfileViewSet,
    ProjectImageViewSet,
    ProjectViewSet,
    SkillViewSet,
    TrainingViewSet,
)

app_name = "cv"

cv_router = routers.DefaultRouter()

cv_router.register("profiles", ProfileViewSet, basename="profile")
cv_router.register("trainings", TrainingViewSet, basename="training")
cv_router.register("experiences", ExperienceViewSet, basename="experience")
cv_router.register("skills", SkillViewSet, basename="skill")
cv_router.register("projects", ProjectViewSet, basename="project")
cv_router.register("images", ProjectImageViewSet, basename="project_image")


urlpatterns = [path("", include(cv_router.urls))]
