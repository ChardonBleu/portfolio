# Generated by Django 5.0.3 on 2024-10-08 14:02

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("cv", "0005_remove_profile_photo"),
    ]

    operations = [
        migrations.AddField(
            model_name="experience",
            name="site",
            field=models.CharField(
                choices=[
                    ("Sur site", "Sur Site"),
                    ("Hybride", "Hybride"),
                    ("A distance", "A distance"),
                ],
                default="Sur site",
                max_length=255,
                verbose_name="Lieu d'emploi",
            ),
        ),
    ]
