# Generated by Django 5.1.2 on 2024-11-05 11:25

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("cv", "0009_training_learned_skills_training_site"),
    ]

    operations = [
        migrations.AddField(
            model_name="training",
            name="logo",
            field=models.ImageField(
                blank=True, null=True, upload_to="", verbose_name="logo de l'organisme"
            ),
        ),
    ]
