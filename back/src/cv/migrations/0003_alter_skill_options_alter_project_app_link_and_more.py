# Generated by Django 5.0.3 on 2024-04-08 10:13

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("cv", "0002_alter_experience_end_date"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="skill",
            options={"ordering": ["-competence_level", "-appetite_level"]},
        ),
        migrations.AlterField(
            model_name="project",
            name="app_link",
            field=models.URLField(blank=True, verbose_name="lien vers l'application"),
        ),
        migrations.AlterField(
            model_name="project",
            name="github_link",
            field=models.URLField(blank=True, verbose_name="lien Git-hub ou Gitlab"),
        ),
    ]
