# Generated by Django 5.1.2 on 2024-11-19 09:17

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("cv", "0013_project_year"),
    ]

    operations = [
        migrations.AlterField(
            model_name="project",
            name="experience",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="projects",
                to="cv.experience",
                verbose_name="Contexte professionnel",
            ),
        ),
    ]
