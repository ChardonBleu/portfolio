from rest_framework import viewsets

from .models import Experience, Profile, Project, ProjectImage, Skill, Training
from .serializers import (
    ExperienceSerializer,
    ProfileSerializer,
    ProjectImageSerializer,
    ProjectSerializer,
    SkillSerializer,
    TrainingSerializer,
)


class ProfileViewSet(viewsets.ModelViewSet):
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    http_method_names = ["get", "head", "options", "trace"]

    def get_object(self):
        return Profile.objects.get(user__first_name="Fabienne", user__last_name="Rondi")


class TrainingViewSet(viewsets.ModelViewSet):
    serializer_class = TrainingSerializer
    http_method_names = ["get", "head", "options", "trace"]

    def get_queryset(self):
        me = Profile.objects.get(user__first_name="Fabienne", user__last_name="Rondi")
        return Training.objects.filter(profile=me).order_by("-delivery_date")


class ExperienceViewSet(viewsets.ModelViewSet):
    serializer_class = ExperienceSerializer
    http_method_names = ["get", "head", "options", "trace"]

    def get_queryset(self):
        me = Profile.objects.get(user__first_name="Fabienne", user__last_name="Rondi")
        return Experience.objects.filter(profile=me).order_by("-start_date")


class SkillViewSet(viewsets.ModelViewSet):
    serializer_class = SkillSerializer
    http_method_names = ["get", "head", "options", "trace"]

    def get_queryset(self):
        me = Profile.objects.get(user__first_name="Fabienne", user__last_name="Rondi")
        return Skill.objects.filter(profile=me)


class ProjectViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectSerializer
    http_method_names = ["get", "head", "options", "trace"]

    def get_queryset(self):
        me = Profile.objects.get(user__first_name="Fabienne", user__last_name="Rondi")
        return Project.objects.filter(profile=me)


class ProjectImageViewSet(viewsets.ModelViewSet):
    serializer_class = ProjectImageSerializer
    http_method_names = ["get", "head", "options", "trace"]

    def get_queryset(self):
        me = Profile.objects.get(user__first_name="Fabienne", user__last_name="Rondi")
        return ProjectImage.objects.filter(project__profile=me)
