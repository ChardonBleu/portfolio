from datetime import date

import pytest
from cv.models import Experience, Profile, Project, ProjectImage, Skill, Training
from django.contrib.auth.models import User
from factory.django import DjangoModelFactory
from rest_framework.test import APIClient


# **************************************************************************
#    Factories
# **************************************************************************
class UserFactory(DjangoModelFactory):
    class Meta:
        model = User

    first_name = "Fabienne"
    last_name = "Rondi"
    email = "user_test@test.com"


class ProfileFactory(DjangoModelFactory):
    class Meta:
        model = Profile


class ExperienceFactory(DjangoModelFactory):
    class Meta:
        model = Experience


class TrainingFactory(DjangoModelFactory):
    class Meta:
        model = Training


class SkillFactory(DjangoModelFactory):
    class Meta:
        model = Skill


class ProjectFactory(DjangoModelFactory):
    class Meta:
        model = Project


class ProjectImageFactory(DjangoModelFactory):
    class Meta:
        model = ProjectImage


# **************************************************************************
#    Fixtures
# **************************************************************************


@pytest.fixture()
@pytest.mark.django_db()
def api_client() -> APIClient:
    """A non authenticated client"""
    return APIClient()


@pytest.fixture()
def admin_client(client, admin_user):
    client.force_login(admin_user)
    return client


@pytest.fixture()
@pytest.mark.django_db()
def test_profile() -> ProfileFactory:
    return ProfileFactory(
        birth_date=date(2024, 1, 1),
        github_link=" ",
        gitlab_link=" ",
        codingame_link=" ",
        linkedin_link=" ",
        user=UserFactory(),
    )


@pytest.fixture()
@pytest.mark.django_db()
def test_past_experience(test_profile) -> ExperienceFactory:
    return ExperienceFactory(
        job="Mon CDD passé",
        start_date=date(2021, 1, 1),
        end_date=date(2021, 12, 31),
        type=Experience.TypeJob.CDD,
        profile=test_profile,
    )


@pytest.fixture()
@pytest.mark.django_db()
def test_current_experience(test_profile) -> ExperienceFactory:
    return ExperienceFactory(
        job="Mon CDI actuel",
        start_date=date(2022, 1, 1),
        type=Experience.TypeJob.CDI,
        profile=test_profile,
    )


@pytest.fixture()
@pytest.mark.django_db()
def test_training(test_profile) -> TrainingFactory:
    return TrainingFactory(
        title="Ma formation préférée",
        delivery_date=date(2015, 6, 1),
        profile=test_profile,
    )


@pytest.fixture()
@pytest.mark.django_db()
def test_skill(test_profile) -> SkillFactory:
    return SkillFactory(
        title="Python", competence_level=10, appetite_level=10, profile=test_profile
    )


@pytest.fixture()
@pytest.mark.django_db()
def test_project(test_profile) -> ProjectFactory:
    return ProjectFactory(title="Mon super blog", profile=test_profile)


@pytest.fixture()
@pytest.mark.django_db()
def test_project_image(test_project) -> ProjectImageFactory:
    return ProjectImageFactory(name="Mon image", project=test_project)
