import pytest
from cv.models import Experience, Profile
from django.urls import reverse
from rest_framework.status import HTTP_200_OK, HTTP_405_METHOD_NOT_ALLOWED
from rest_framework.test import APIClient


@pytest.mark.django_db()
def test_profile_list(api_client: APIClient, test_profile: Profile):
    url = reverse("cv:profile-detail", args=[1])
    response = api_client.get(url, format="json")

    assert response.status_code == HTTP_200_OK


@pytest.mark.django_db()
def test_profile_post(api_client: APIClient, test_profile: Profile):
    url = reverse("cv:profile-list")
    response = api_client.post(url, format="json")

    assert response.status_code == HTTP_405_METHOD_NOT_ALLOWED


@pytest.mark.django_db()
def test_experience_list(
    api_client: APIClient, test_past_experience: Experience, test_profile: Profile
):
    url = reverse("cv:experience-list")
    response = api_client.get(url, format="json")

    assert response.status_code == HTTP_200_OK


@pytest.mark.django_db()
def test_experience_post(
    api_client: APIClient, test_past_experience: Experience, test_profile: Profile
):
    url = reverse("cv:experience-list")
    response = api_client.post(url, format="json")

    assert response.status_code == HTTP_405_METHOD_NOT_ALLOWED


@pytest.mark.django_db()
def test_training_list(api_client: APIClient, test_profile: Profile):
    url = reverse("cv:training-list")
    response = api_client.get(url, format="json")

    assert response.status_code == HTTP_200_OK


@pytest.mark.django_db()
def test_training_post(api_client: APIClient, test_profile: Profile):
    url = reverse("cv:training-list")
    response = api_client.post(url, format="json")

    assert response.status_code == HTTP_405_METHOD_NOT_ALLOWED


@pytest.mark.django_db()
def test_skill_list(api_client, test_skill, test_profile):
    url = reverse("cv:skill-list")
    response = api_client.get(url, format="json")

    assert response.status_code == HTTP_200_OK


@pytest.mark.django_db()
def test_skill_post(api_client, test_skill, test_profile):
    url = reverse("cv:skill-list")
    response = api_client.post(url, format="json")

    assert response.status_code == HTTP_405_METHOD_NOT_ALLOWED


@pytest.mark.django_db()
def test_project_list(api_client, test_project, test_profile):
    url = reverse("cv:project-list")
    response = api_client.get(url, format="json")

    assert response.status_code == HTTP_200_OK


@pytest.mark.django_db()
def test_project_post(api_client, test_project, test_profile):
    url = reverse("cv:project-list")
    response = api_client.post(url, format="json")

    assert response.status_code == HTTP_405_METHOD_NOT_ALLOWED


@pytest.mark.django_db()
def test_project_image_list(api_client, test_project_image, test_profile):
    url = reverse("cv:project_image-list")
    response = api_client.get(url, format="json")

    assert response.status_code == HTTP_200_OK
