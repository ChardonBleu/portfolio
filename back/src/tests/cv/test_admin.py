from django.conf import settings
from rest_framework import status


def test_admin_url(admin_client):
    url = f"/{settings.ADMIN_URL}"
    response = admin_client.get(url)

    assert response.status_code == status.HTTP_200_OK
