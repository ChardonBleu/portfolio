import pytest
from cv.models import Experience, Profile, Project, ProjectImage, Skill, Training


@pytest.mark.django_db()
def test_profile_str(test_profile: Profile):
    assert test_profile.__str__() == "Fabienne Rondi"


@pytest.mark.django_db()
def test_past_experience_str(test_past_experience: Experience):
    assert test_past_experience.__str__() == "Mon CDD passé de 2021-01-01 à 2021-12-31"


@pytest.mark.django_db()
def test_current_experience_str(test_current_experience: Experience):
    assert (
        test_current_experience.__str__()
        == "Mon CDI actuel de 2022-01-01 à aujourd'hui"
    )


@pytest.mark.django_db()
def test_training_str(test_training: Training):
    assert test_training.__str__() == "Ma formation préférée 2015-06-01"


@pytest.mark.django_db()
def test_skill_str(test_skill: Skill):
    assert test_skill.__str__() == "Python"


@pytest.mark.django_db()
def test_project_str(test_project: Project):
    assert test_project.__str__() == "Mon super blog"


@pytest.mark.django_db()
def test_project_image_str(test_project_image: ProjectImage):
    assert test_project_image.__str__() == "Mon image"
