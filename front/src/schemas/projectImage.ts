import { z } from 'zod'

export const projectImageSchema = z.object({
  id: z.number(),
  image: z.string().url({ message: 'Invalid url' }).nullable(),
  project: z.number(),
})

export type ProjectImage = z.infer<typeof projectImageSchema>
