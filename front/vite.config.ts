import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

export default defineConfig(() => {
  const plugins = [vue()]
  return {
    plugins,
    build: {
      sourcemap: true, // Source map generation must be turned on
    },
    server: {
      proxy: {
        '/api': {
          target: 'http://127.0.0.1:3000',
          changeOrigin: true,
        },
      },
    },
    optimizeDeps: {
      include: ['d3'],
    },
  }
})
