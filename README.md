# PortFolio Fabrodev

This website is my portfolio. It presents my background and experience in back-end and 
front-end development.
This Web App has been made with Django and Vue.js frameworks.

## Back-end Part:

Back-end part is a little API made with Django Rest Framework and PostgreSQL.
I chose poetry as dependencies manager and i used docker as containers manager.  
[README back-end documentation](back/README.md)

## Front-end Part:

Front-end is a Vue app using Vue.js and Typescript.
[README front-end documentation](front/README.md)

## Scripts folder:

Contains scripts to init issue and to make conventional commit

- **./init-issue.sh**:  
    Script check for the current branch. If not main, it quits.  
    Check for staged changes on current main. If not, ask user to stage changes or not.
    Ask fot suffix branch name: issue number or issue name.  
    Create new branch with name PF-<suffix> and checkout on it.  


- **./conventional-commit.sh**:  
    Scripts create commit with icon, type and description.  
