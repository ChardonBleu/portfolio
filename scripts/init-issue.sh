#!/bin/bash
set -e

current_branch=$(git branch --show-current)
echo $current_branch
if [ "$current_branch" != "main" ]
then
    echo "You are not on main branch ! "
    exit 1
fi
if [[ -z $(git --no-pager diff --staged) ]]; then
    yes=""
    read -p "no changes detected, do you want to add ALL changes (y/N) ?" yes
    if [[ "$yes" == "y" ]]
    then
        git add -A
    fi
fi

dir=$(dirname $0)
type=""
desc=""
jira_issue=""

while getopts t:d:j: opt
do
    case "$opt"
        in
        t) type="$OPTARG";;
        d) desc="$OPTARG";;
        j) jira_issue="$OPTARG";;
    esac
done

while [ -z $jira_issue ]
do
    read -p "jira issue number (without PF) or suffix branch name: " jira_issue
done

args=""
is_number="^[0-9]+$"

[ -n $jira_issue ] && [[ $jira_issue =~ $is_number ]] && args="-j $jira_issue "
[[ -n "$desc" ]] && args="$args -d $desc "
[ -n "$type" ] && args="$args -t $type"

git checkout -b "PF-$jira_issue"

if [[ -z $(git --no-pager diff --staged) ]]; then
    echo "no changes detected, only the branch was created."
    exit 0
fi
